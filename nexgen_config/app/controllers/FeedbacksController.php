<?php

class FeedbacksController extends \BaseController {


	public function index()
	{
		$feedbacks = Feedbacks::all();
		return Response::json($feedbacks);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	public function upload(){

		if ( !empty( $_FILES ) ) {

		    $tempPath = $_FILES[ 'file' ][ 'tmp_name' ];
		    // $uploadPath = storage_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . $_FILES[ 'file' ][ 'name' ];
			$uploadPathT = 'assets' . "/" . 'images' . "/" . 'uploads' . "/" . $_FILES[ 'file' ][ 'name' ];

		    move_uploaded_file( $tempPath, $uploadPathT );

		    $answer = array( 'answer' => 'File transfer completed', 'FilePath' => $uploadPathT );
		    $json = json_encode( $answer );

			//Log::error($exception);
		    echo $json;

		} else {

		    echo 'No files';
		}
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
		$input = Input::all();
		// $user_id = Request :: input("user_id");
  //       $logo =Request :: input("logo");
  //       $fb = Request :: input("fb");

		try{

			$c = new Feedbacks();
			$c->logo		= $input['logo'];
			$c->fb			= $input['fb'];


			if($c->save())
				return Response::json(array('success' => true,"message"=>'Feedbacks Saved successfully!'));
			else
				throw new \Exception("Could not save the Feedbacks");
		} catch(\Exception $e){
			return Response::json(array('success' => false,'message'=>$e->getMessage(),'input'=>$input));
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$feedbacks = Feedbacks::find($id);
		return Response::json($feedbacks);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$c =  Feedbacks::find($id);
		$input = Input::all();

		try{

			$c->logo		= $input['logo'];
			$c->fb			= $input['fb'];
            
                      
                                
			if($c->save())
				return Response::json(array('success' => true,'id'=>$c->id));
			else
				throw new \Exception("Could not save the Feedbacks");

		} catch(\Exception $e){
			return Response::json(array('success' => false,'message'=>$e->getMessage(),'input'=>$input));
		}

	}

	
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
	if(Feedbacks::find($id)->delete())
		return Response::json(array('success' => true));
	else
	 	return Response::json(array('success' => false));
	}


}
