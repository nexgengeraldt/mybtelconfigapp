<?php

class ProposalsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$proposals = Proposals::all();
		return Response::json($proposals);

	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $user_id = Request :: input("user_id");
        $files = Request :: input("file");
        $companyName = Request :: input("companyName");
        $businessName = Request :: input("businessName");
        $contactNumber = Request :: input("contactNumber");
        $emailAddress = Request :: input("emailAddress");
        $display = Request :: input("display");
        $schQty = Request :: input("schQty");
        $schOption = Request :: input("schOption");
        $hTerm = Request :: input("hTerm");
        $bTerm = Request :: input("bTerm");
        $nationalCall = Request :: input("nationalCall");
        $localCall = Request :: input("localCall");
        $callNumbers = Request :: input("callNumbers");
        $diD = Request :: input("diD");
        $feedbacks = Request :: input("feedbacks");

		// $schOption = serialize($schOption);
		// $schQty = serialize($schQty);
		// $feedbacks = serialize($feedbacks);
        $editId = Request :: input("editId");

        // $fileName = serialize(Request::input("fileName"));
        if ($editId) {
            $fd = Proposals::find($editId);
        } 
  //       if (Proposals::where('user_id', '=', $user)->exists()) {
		// 	$fd = Proposals::find($user);
		// }
        else {
            $fd = new Proposals();
        }

        try {
            $fd->user_id = $user_id;
            $fd->files = '';
            $fd->companyName		= $companyName;
			$fd->businessName		= $businessName;
			$fd->contactNumber 		= $contactNumber;
			$fd->emailAddress 		= $emailAddress;
			$fd->display 			= $display;
			$fd->schQty 			= serialize($schQty);
			$fd->schOption 			= serialize($schOption);
			$fd->hTerm 				= $hTerm;
			$fd->bTerm 				= $bTerm;
			$fd->nationalCall 		= $nationalCall;
			$fd->localCall 			= $localCall;
			$fd->callNumbers 		= $callNumbers;
			$fd->diD 				= $diD;
			$fd->feedbacks 			= serialize($feedbacks);

			$fd->created_at 		= time();
			$fd->updated_at 		= time();

            $fd->save();

            return Response::json(array("success" => "success", "editId"=> $fd->id));
        } catch (Exception $ex) {
            return Response ::json(array("success" => "false", "message" => $ex->getMessage()));
        }





		// $input = Input::all();

	 //    try{
		// 	$c = new Proposals();
		// 	$schOption = serialize($input['schOption']);
		// 	$feedbacks = serialize($input['feedbacks']);

		// 	// $c->user_id 			= $input['user_id'];
		// 	// $c->files 				= $input['files'];
		// 	$c->companyName			= $input['companyName'];
		// 	$c->businessName		= $input['businessName'];
		// 	$c->contactNumber 		= $input['contactNumber'];
		// 	$c->emailAddress 		= $input['emailAddress'];
		// 	$c->display 			= $input['display'];
		// 	$c->schQty 				= $input['schQty'];
		// 	$c->schOption 			= $schOption;
		// 	$c->hTerm 				= $input['hTerm'];
		// 	$c->bTerm 				= $input['bTerm'];
		// 	$c->nationalCall 		= $input['nationalCall'];
		// 	$c->localCall 			= $input['localCall'];
		// 	$c->callNumbers 		= $input['callNumbers'];
		// 	$c->diD 				= $input['diD'];
		// 	$c->feedbacks 			= $feedbacks;

		// 	$c->created_at 		= time();
		// 	$c->updated_at 		= time();


		// 	if($c->save())
		// 		return Response::json(array('success' => true,'id'=>$c->id,"message"=>'Proposal saved successfully'));
		// 	else
		// 		throw new \Exception("Could not save the Proposal");

	 //   } catch (\Exception $e){
		// 	return Response::json(array('success' => false,'message'=>$e->getMessage()));
	 //   }

	}



	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$proposals = Proposals::find($id);
		return Response::json($proposals);

	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		// $input = Input::all();
		// $c     = SalesRep::find($id);

	 //    try{

		// 	$c->name 			= $input['name'];
		// 	$c->phone 			= $input['phone'];
		// 	$c->email 			= $input['email'];

		// 	$c->updated_at 		= time();


		// 	if($c->save())
		// 		return Response::json(array('success' => true,'id'=>$c->id,"message"=>'Salesrep saved successfully'));
		// 	else
		// 		throw new \Exception("Couldnot save the salesrep");

	 //   } catch (\Exception $e){
		// 	return Response::json(array('success' => false,'message'=>$e->getMessage()));
	 //   }

	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		 if(Proposals::find($id)->delete())
			return Response::json(array('success' => true));
		 else
		 	return Response::json(array('success' => false));

	}


}
