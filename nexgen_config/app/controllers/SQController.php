<?php
class SQController extends \BaseController
{
    public function getLocationIds(){
      $input = Input::all();
      $soapArr = Config::get('app.frontier');
      $_xmlns = $soapArr['xsd'];
      
      try {
        $client = new SoapClient($soapArr['wsdl'], $soapArr);
        $request = "<FindServiceProviderLocationIdRequest xmlns='$_xmlns'><address><streetNumber>" . $input['streetNumber'] . "</streetNumber><streetName>" . $input['streetName'] . "</streetName><suburb>" . $input['suburb'] . "</suburb><state>" . $input['state'] . "</state><postcode>" . $input['postcode'] . "</postcode></address></FindServiceProviderLocationIdRequest>";

        $response = $client->findServiceProviderLocationId(new SoapVar($request,XSD_ANYXML));
        //$locations = $response;
        $locations = array();
        $serviceProviders = $response->serviceProviderLocationList->serviceProviderLocationList;
        if (is_array($serviceProviders)){
            foreach ($serviceProviders as $providers){
            //$locations[] = $providers->locationList;
                if (isset($providers->locationList->addressInformation)){
                    $locationList = $providers->locationList->addressInformation;
                    if (is_array($locationList)){
                      foreach($locationList as $address){
                        $locationArray = array (
                          'address' => $address->address,
                          'displayAddress' => $address->displayAddress,
                          'locationId' => $address->locationId,
                          'serviceProvider' => $address->serviceProvider
                        );
                        $locations[] = $locationArray;
                      }
                    } else {
                      $locationArray = array (
                        'address' => $locationList->address,
                        'displayAddress' => $locationList->displayAddress,
                        'locationId' => $locationList->locationId,
                        'serviceProvider' => $locationList->serviceProvider
                      );
                      $locations[] = $locationArray;
                    }
                }
            }
        } else {
            if (isset($providers->locationList->addressInformation)){
              $locationList = $serviceProvider->locationList->addressInformation;
              if (is_array($locationList)){
                foreach($locationList as $address){
                  $locationArray = array (
                    'address' => $address->address,
                    'displayAddress' => $address->displayAddress,
                    'locationId' => $address->locationId,
                    'serviceProvider' => $address->serviceProvider
                  );
                  $locations[] = $locationArray;
                }
              } else {
                $locationArray = array (
                  'address' => $locationList->address,
                  'displayAddress' => $locationList->displayAddress,
                  'locationId' => $locationList->locationId,
                  'serviceProvider' => $locationList->serviceProvider
                );
                $locations[] = $locationArray;
              }
            }
        }
        return Response::json(array('success'=>'success','response'=>$locations,'api_response'=>$response));
        //return Response::json(array('success'=>'success','response'=>$response));
      } catch (Exception $e){
        return Response::json(array('error'=>'error','message'=>$e));
      }
    }

    public function getLocationId(){
      $input = Input::all();
      $soapArr = Config::get('app.frontier');
      $_xmlns = $soapArr['xsd'];
      
      try {
        $client = new SoapClient($soapArr['wsdl'], $soapArr);
        $request = "<FindServiceProviderLocationIdRequest xmlns='$_xmlns'><address><streetNumber>"  . $input['streetNumber'] . "</streetNumber><streetName>" 
            . $input['streetName'] . "</streetName><suburb>" 
            . $input['suburb'] . "</suburb><state>" 
            . $input['state'] . "</state><postcode>" 
            . $input['postcode'] . "</postcode></address></FindServiceProviderLocationIdRequest>";

        $response = $client->findServiceProviderLocationId(new SoapVar($request,XSD_ANYXML));
        //$locations = $response;
        $locations = array();
        $serviceProviders = $response->serviceProviderLocationList->serviceProviderLocationList;
        // $serviceProviders = $response;
        if (is_array($serviceProviders)){
            foreach ($serviceProviders as $providers){
            //$locations[] = $providers->locationList;
                if (isset($providers->locationList->addressInformation)){
                    $locationList = $providers->locationList->addressInformation;
                    if (is_array($locationList)){
                      foreach($locationList as $address){
                        $locationArray = array (
                          'address' => $address->address,
                          'displayAddress' => $address->displayAddress,
                          'locationId' => $address->locationId,
                          'serviceProvider' => $address->serviceProvider
                        );
                        $locations[] = $locationArray;
                      }
                    } else {
                      $locationArray = array (
                        'address' => $locationList->address,
                        'displayAddress' => $locationList->displayAddress,
                        'locationId' => $locationList->locationId,
                        'serviceProvider' => $locationList->serviceProvider
                      );
                      $locations[] = $locationArray;
                    }
                }
            }
        } else {
            if (isset($providers->locationList->addressInformation)){
              $locationList = $serviceProvider->locationList->addressInformation;
              if (is_array($locationList)){
                foreach($locationList as $address){
                  $locationArray = array (
                    'address' => $address->address,
                    'displayAddress' => $address->displayAddress,
                    'locationId' => $address->locationId,
                    'serviceProvider' => $address->serviceProvider
                  );
                  $locations[] = $locationArray;
                }
              } else {
                $locationArray = array (
                  'address' => $locationList->address,
                  'displayAddress' => $locationList->displayAddress,
                  'locationId' => $locationList->locationId,
                  'serviceProvider' => $locationList->serviceProvider
                );
                $locations[] = $locationArray;
              }
            }
        }
        return Response::json(array('success'=>'success','response'=>$locations,'api_response'=>$response));
        //return Response::json(array('success'=>'success','response'=>$response));
      } catch (Exception $e){
        return Response::json(array('error'=>'error','message'=>$e));
      }
    }


    // public function getFnn()
    // {
    //   $input = Input::all();
    //   $soapArr = Config::get('app.frontier');
    //   $_xmlns = $soapArr['xsd'];
    //   try {
    //     $client = new SoapClient($soapArr['wsdl'], $soapArr);
    //   }
    //   return Response::json(array('success'=>'success','response'=>'api_response'));
    //   catch{

    //   }
    // }

    public function getServiceQualification()
    {
      $input = Input::all();
      $soapArr = Config::get('app.frontier');
      $_xmlns = $soapArr['xsd'];
      try {
        $client = new SoapClient($soapArr['wsdl'], $soapArr);
        if ($input['serviceProvider']=='NBN'){
          $request = "<QualifyProductRequest xmlns='$_xmlns'><qualifyNationalWholesaleBroadbandProductRequest><nbnLocationID>" . $input['locationId'] . "</nbnLocationID><returnExtendedNbnSqData>true</returnExtendedNbnSqData></qualifyNationalWholesaleBroadbandProductRequest></QualifyProductRequest>";
        }elseif ($input['serviceProvider']=='Telstra'){
          $request = "<QualifyProductRequest xmlns='$_xmlns'><qualifyNationalWholesaleBroadbandProductRequest><telstraLocationID>" . $input['locationId'] . "</telstraLocationID></qualifyNationalWholesaleBroadbandProductRequest></QualifyProductRequest>";
        }
        elseif ($input['serviceProvider']=='AAPT'){
          $request = "<QualifyELanProductRequest  xmlns='$_xmlns'><qualifyNationalWholesaleBroadbandProductRequest><aaptLocationID>" . $input['locationId'] . "</aaptLocationID></qualifyNationalWholesaleBroadbandProductRequest></QualifyELanProductRequest >";
        }
        
        elseif($input['serviceNumber'] && $input['serviceProvider']==''){
          $request = "
          <QualifyProductRequest xmlns='$_xmlns'>
          <qualifyNationalWholesaleBroadbandProductRequest>
          <endCSN >".$input['serviceNumber']."</endCSN >
          </qualifyNationalWholesaleBroadbandProductRequest>
          </QualifyProductRequest>";
        }
         
        $response = $client->qualifyProduct(new SoapVar($request, XSD_ANYXML));
        $accessQualificationList = $response->accessQualificationList;
        $qualifyList = array();
        if ($input['serviceProvider']=='NBN'){
          if (is_array($accessQualificationList)){
            foreach ($accessQualificationList as $qualify){
              $qualifyRecord = array(
                'id' => $qualify->id,
                'qualificationResult' => $qualify->qualificationResult,
                'accessMethod' => $qualify->accessMethod,
                'accessType' => $qualify->accessType,
                'availableServiceSpeeds' =>$qualify->availableServiceSpeeds
              );
              $qualifyList[] = $qualifyRecord;
            }
          } else {
            $qualifyRecord = array(
              'id' => $accessQualificationList->id,
              'qualificationResult' => $accessQualificationList->qualificationResult,
              'accessMethod' => $accessQualificationList->accessMethod,
              'accessType' => $accessQualificationList->accessType,
              'availableServiceSpeeds' =>$accessQualificationList->availableServiceSpeeds
            );
            $qualifyList[] = $qualifyRecord;
          }
        }
        elseif ($input['serviceProvider']=='TELSTRA'){
          if (is_array($accessQualificationList)){
            foreach ($accessQualificationList as $qualify){
              $qualifyRecord = array(
                'id' => $qualify->id,
                'qualificationResult' => $qualify->qualificationResult,
                'accessMethod' => $qualify->accessMethod,
                // 'accessType' => $qualify->accessType,
                'availableServiceSpeeds' =>$qualify->availableServiceSpeeds
              );
              $qualifyList[] = $qualifyRecord;
            }
          } else {
            $qualifyRecord = array(
              'id' => $accessQualificationList->id,
              'qualificationResult' => $accessQualificationList->qualificationResult,
              'accessMethod' => $accessQualificationList->accessMethod,
              // 'accessType' => $accessQualificationList->accessType,
              'availableServiceSpeeds' =>$accessQualificationList->availableServiceSpeeds
            );
            $qualifyList[] = $qualifyRecord;
          }
        }
        else{
          if (is_array($accessQualificationList)){
            foreach ($accessQualificationList as $qualify){
              $qualifyRecord = array(
                'id' => $qualify->id,
                'qualificationResult' => $qualify->qualificationResult,
                'accessMethod' => $qualify->accessMethod,
                // 'accessType' => $qualify->accessType,
                'availableServiceSpeeds' =>$qualify->availableServiceSpeeds
              );
              $qualifyList[] = $qualifyRecord;
            }
          } else {
            $qualifyRecord = array(
              'id' => $accessQualificationList->id,
              'qualificationResult' => $accessQualificationList->qualificationResult,
              'accessMethod' => $accessQualificationList->accessMethod,
              'accessType' => $accessQualificationList->accessType,
              'availableServiceSpeeds' =>$accessQualificationList->availableServiceSpeeds
            );
            $qualifyList[] = $qualifyRecord;
          }
        }
        return Response::json(array('success'=>'success','response'=>$qualifyList,'api_response'=>$response));
        // return Response::json(array('success'=>'success','response'=>$response));
      } catch (Exception $e){
        return Response::json(array('error'=>'error','message'=>$e));
      }
      //$path = Config::get('app.path');
    //   $wsdl = __DIR__ . '/FrontierLink.wsdl';

    //   $wsdl;

    //   $soapArr = array(
    //     "soap_version" => SOAP_1_1,
    //     "trace" => true,
    //     "exceptions" => true,
    //     "local_cert" => __DIR__ . '/frontierlink-cert.nexgen.com.au.p12',
    //     "passphrase" => "PJLerbNf",
    //     "location" => "https://frontierlink-cert2.aapt.com.au:8482/FrontierLink/services/ServiceQualification",
    //     "style" => SOAP_DOCUMENT,
    //     "use" => SOAP_LITERAL,
    //     "cache_wsdl" => "WSDL_CACHE_NONE"
    //   );
       
    //   try {
    //     $client = new SoapClient($wsdl, $soapArr);
    //     //echo '<br />Client setup<br /><pre>';
    //     $_xmlns = __DIR__ . '/xsd';
    //     $request = "<QualifyProductRequest xmlns='$_xmlns'><qualifyNationalWholesaleBroadbandProductRequest><nbnLocationID>LOC000010294379</xsd:nbnLocationID><returnExtendedNbnSqData>true<returnExtendedNbnSqData><qualifyNationalWholesaleBroadbandProductRequest></QualifyProductRequest>";
         
    //     $response = $client->qualifyProduct(new SoapVar($request, XSD_ANYXML));
    //     //echo '<br />Request initiated';
    //     //var_dump($response);
    //     return Response::json(array('success'=>'success','dir'=>$wdsl,'response'=>$response));
    //   } catch (Exception $e) {
    //     return Response::json(array('error'=>'error','dir'=>$wsdl,'response'=>$e));
    //   }
      
    }

    public function to_Csv(){
      $csv = Input::all();
      $headers = array (
        'Content-Type' => 'application/csv',
        'Content-Disposition' => 'attachment; filename=sq.csv',
        'Pragma' => 'no-cache'
      );
      $file = __DIR__ . '/../../../public_html/assets/files/sq.csv';
      $fp = fopen($file, 'w');
      // // $fp = fopen('php://output', 'w');
      fputcsv($fp, array('Location ID','Address','Service Provider'));
      foreach ($csv['addresses'] as $value) {
        $valueArray = array(
          'locationId'=>$value['locationId'],
          'displayAddress'=>$value['displayAddress'],          
          'serviceProviders'=>$value['serviceProvider']
        );
        fputcsv($fp, $valueArray);
      }      
      fclose($fp);
      return Response::json(array("success"=>"success","filename"=>"sq.csv")); 
    }
}