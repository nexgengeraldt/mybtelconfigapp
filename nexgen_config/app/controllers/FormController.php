<?php
class FormController extends \BaseController
{
    public function createPdf()
    {
        set_time_limit(0);
        $forms = Request :: input("htmlData");
        $userDetails = Request :: input("userDetails");
        $account = Request :: input("account");
        $fromEmail = "noreply@btelconfigapp.xyz";
        $fromName = "Business Telecom";
        $subject = 'Welcome to Business Telecom';
        $toEmail = $userDetails["email"];
        $toName = $userDetails["name"];
        $accEmail = $account['email'];
        $accName = $account['name'];
        $mailData = [
            'user' => $toName,
            'userEmail' => $toEmail,
            'name' => $fromName,
            'salesRepName' => $accName,
            'email'=> $fromEmail
        ];

        $paths = array();

        if (Auth::user()) {
            $managerEmail = explode(";",Auth::user()->manageremail);
        }

        // if (is_array($forms) || is_object($forms)) {
        //     foreach ($forms as $form) {
        //         $pdf = new mPDF('utf-8', 'A4');
        //         $pdf->WriteHTML($form['html']);
        //         $data = array("pdf" => $pdf->Output("assets/files/".$form['fileName']));
        //         $paths[] = "assets/files/".$form['fileName'];
        //     }
        // }


        foreach($forms as $form) {
          $mpdf = new \Mpdf\Mpdf([
              'mode' => 'utf-8',
              'format' => 'A4',
              'orientation' => 'P',

          ]);
          $mpdf->packTableData = true;
          // Buffer the following html with PHP so we can store it to a variable later
          ob_start();
          // This is where your script would normally output the HTML using echo or print
          echo $form['html'];
          // Now collect the output buffer into a variable
          $html = ob_get_contents();
          ob_end_clean();
          // send the captured HTML from the output buffer to the mPDF class for processing
          $mpdf->WriteHTML($html);
          $data = array("pdf" => $mpdf->Output("assets/files/".$form['fileName']));
          $paths[] = "assets/files/".$form['fileName'];
      }
        if (strpos($form['fileName'],'draft')!==false){
          $emailTemplate = 'emails.form.draftforms';
        } else {
          $emailTemplate = 'emails.form.forms';
        }
        
        // Mail::send($emailTemplate, $mailData, function($message) use ($subject, $paths, $accEmail, $managerEmail)
        // {
        //     $message->to($accEmail);
        //     if ($accEmail!='info@somelongcompany.com.au'){
        //       $message->cc('JamesHarb@nexgen.com.au')
        //             ->cc('ElieAyoub@nexgen.com.au')
        //             ->cc('developer@businesstelecom.com.au')
        //             ->cc('QA@nexgen.com.au')
        //             ->cc('AnnaKrjatian@businesstelecom.com.au');
        //     }
        //     else {
        //       $message->cc('JamesHarb@nexgen.com.au')
        //             ->cc('ElieAyoub@nexgen.com.au')
        //             ->cc('developer@businesstelecom.com.au')
        //             ->cc('AnnaKrjatian@businesstelecom.com.au')
		    // ->cc('LauraFerraro@businesstelecom.com.au');
        //     }
            
        //     foreach ($managerEmail as $manager){
        //       $message->cc($manager);
        //     }

        //     $message->subject($subject);

        //     foreach ($paths as $path) {
        //         $message->attach($path);
        //     }
        // });

        if (Mail::failures()) {
            Log::debug(Mail::failures());
            return Response::json(array("success" => "fail"));
        } else {
            return Response::json(array("success" => "success","managerEmail"=>$managerEmail));
        }
    }

    public function saveData()
    {
        $data = Request :: input("data");
        $user = Request :: input("user");
        $status = Request :: input("type");

         //No credit card info to be saved in the server.
        isset($data["aCardNumber"]) ? $data["aCardNumber"] = "" : "";
        isset($data["aCardExpiry"]) ? $data["aCardExpiry"] = "" : "";
        isset($data["aCardName"]) ? $data["aCardName"] = "" : "";
        isset($data["aCardType"]) ? $data["aCardType"] = "" : "";

        $formData = serialize($data);
        $editId = Request :: input("editId");

        $fileName = serialize(Request::input("fileName"));
        if ($editId) {
            $fd = FormData::find($editId);
        } else {
            $fd = new FormData();
            $fd->user_id = $user;
        }

        try {
            // $fd->user_id = $user;
            $fd->form_data = $formData;
            $fd->status = $status;
            $fd->files = $fileName;

            $fd->save();

            return Response::json(array("success" => "success", "editId"=> $fd->id));
        } catch (Exception $ex) {
            return Response ::json(array("success" => "false", "message" => $ex->getMessage()));
        }
    }

    public function downloadForms()
    {
        $id = Request :: input("id");
        if ($id) {
           $data = FormData::find($id);
           $files = unserialize($data['files']);
           return Response::json(array("success" => "success", "files" => $files));
        }

    }
    public function toCsv(){
      $referals = Request :: input("referals");
      $headers = array (
        'Content-Type' => 'application/csv',
        'Content-Disposition' => 'attachment; filename=referals.csv',
        'Pragma' => 'no-cache'
      );
      $file = __DIR__ . '/../../../public_html/assets/files/referals.csv';
      $fp = fopen($file, 'w');
      // // $fp = fopen('php://output', 'w');
      fputcsv($fp, array('Company Name','Contact Person','Telephone Number','Refered By','Created Date'));
      foreach ($referals as $fields) {
        for ($i=0;$i<$fields['referal_count'];$i++){
          fputcsv($fp, array(isset($fields['rBusinessName'][$i])?$fields['rBusinessName'][$i]:'',isset($fields['rContactPerson'][$i])?$fields['rContactPerson'][$i]:'',isset($fields['rPhoneNumber'][$i])?$fields['rPhoneNumber'][$i]:'',$fields['name']),$fields['created_at']);
        }  
      }
      fclose($fp);
      return Response::json(array("success"=>"success","filename"=>"referals.csv","referals"=>$referals)); 
    }

    public function sendReferrals(){
        $lp_Comments = Request :: input("lp_Comments");
        $Company = Request :: input("lp_Company");
        $ContactFirstName = Request :: input("lp_ContactFirstName");
        $Phone = Request :: input("lp_Phone");
        //$lp_UserField6 = Request :: input("Referral From Ap");
        $wl_referredby = Request :: input("wl_referredby"); 
        $wl_salesrep = Request :: input("wl_salesrep");
        $companyCount = sizeof($Company);
        $paramArray = array();
        for ($i=0;$i<$companyCount;$i++){
          $lp_Company = $Company[$i];
          $lp_Phone = $Phone[$i];
          $lp_ContactFirstName = $ContactFirstName[$i];
          if ($lp_Company!="" || $lp_Phone!="" || $lp_ContactFirstName!=""){ 
            $params = array(
              'lp_UserField6' => "Referral From Ap Business Telecom",              
              'lp_Company' => $lp_Company,
              'lp_CompanyID' => '12498',
              'lp_Comments' => $lp_Comments,
              'lp_ContactFirstName' => $lp_ContactFirstName,
              'lp_Phone' => $lp_Phone,
              'lp_Password' => '8J4xM13diy',
              'wl_referredby' => $wl_referredby,
              'lp_Username' => 'NexgenBTELTMSG',
              'wl_salesrep' => $wl_salesrep,
              'wl_leadsource' => 2                 
            );
            $paramArray[] = $params;
            $options = array(
              CURLOPT_URL => 'https://www.crmtool.net/lp_NewLead.asp', 
              CURLOPT_POST => 1,
              CURLOPT_POSTFIELDS => http_build_query($params),
              CURLOPT_RETURNTRANSFER => 1,
            );
            $ch = curl_init(); 
            curl_setopt_array ($ch, $options);  
            $output = curl_exec($ch); 
            curl_close($ch);  
            $responses[] = $output;
            $urlArray[] = $params;
          }
        }
      return Response::json(array("success"=>"success","response"=>$responses,"link"=>$urlArray,"paramArray"=>$paramArray)); 
      // var_dump($name);
    }
    

    public function viewReferals(){
      $id = Request :: input("id");
      if ($id) {
        $data = FormData::find($id);
        $created_date = $data['created_at'];
        $data = unserialize($data['form_data']);
        $referalArray = array (
          'cName' => $data['cName'],
          'cContact' => $data['cContact'],
          'cTel' => $data['cTel'],
          'referals'=> $data['referals'],
          'rBusinessName'=> $data['rBusinessName'],
          'rContactPerson' => $data['rContactPerson'],
          'rPhoneNumber' => $data['rPhoneNumber'],
          'created_at' => date('Y-m-d',strtotime($created_date))
        );
        return Response::json(array("success" => "success", "id" => $id , "data" => $referalArray));
      }
    }

    
    public function getRates(){
        $term = Request :: input("term");
        $type = Request :: input("type");
        $data = Rates::where('term', '=', $term)
                       ->where('type', '=', $type)
                       ->orderBy('effective_at', 'desc')
                       ->first(['rates']);
        // $returnData = array();
        $rates = unserialize($data['rates']);
        return Response::json(array("success"=>"success","rates"=>$rates));
    } 
    
    public function viewAll(){  
        $data = DB::table('form_data')
                        ->select('form_data.id','user_id','form_data','name','form_data.created_at')
                        ->join('users','users.id','=','user_id')
                        ->where('form_data.form_data','like','%referals%')
                        ->where('form_data.status','=','Completed')
                        ->orderBy('created_at', 'asc')
                        ->get();
        $returnData = array();
        foreach ($data as $row){
          $processedData = unserialize($row->form_data);
          $businessNames = array();
          $contactPersons = array();
          $phoneNumbers = array();
          if(count($processedData['referals']) > 0 ){
              foreach ($processedData['rBusinessName'] as $businessName){
                if (!is_null($businessName) || $businessName!='')
                  $businessNames[] = $businessName;
              }
              foreach ($processedData['rContactPerson'] as $contactPerson){
                if (!is_null($contactPerson) || $contactPerson!='')
                  $contactPersons[] = $contactPerson;
              }
              foreach ($processedData['rPhoneNumber'] as $phoneNumber){
                if (!is_null($phoneNumber) || $phoneNumber!='')
                  $phoneNumbers[] = $phoneNumber;
              }
              if (count($businessNames)>0){
                $dataArray = array(
                  'user_id' => $row->user_id,
                  'id' => $row->id,
                  'name' => $row->name,
                  // 'status' => $row->status,
                  'created_at' => $row->created_at,
                  'referals' => array_filter($processedData['referals']),          
                  'rBusinessName' => $businessNames,
                  'rContactPerson' => $contactPersons,
                  'rPhoneNumber' => $phoneNumbers,
                  'referal_count' => count($businessNames)
                );
                $returnData[] = $dataArray;
              }
          }
        }
        return Response::json(array("success" => "success", "dbData"=>$processedData, "response" => $returnData));
    }

    public function showForms()
    {
        $id = Request :: input("id");
        $user_id = Request :: input("user");
        if ($id) {
           $data = FormData::find($id);
           $forms['data'] = unserialize($data['form_data']);
           $forms['id'] = $data['id'];
           return Response::json(array("success" => "success", "formData" => $forms));
        } else {
            $userStatus = User::find($user_id)["type"];
            if ($userStatus == "admin") {
                $data = DB::table('form_data')->orderBy('created_at', "desc")
                                              ->get();
            } else {
                $data = DB::table('form_data')->orderBy('created_at', "desc")
                                              ->where('user_id', $user_id)
                                          ->get();
            }
            $forms = array();
            foreach ($data as $d) {
               $item['creator_id'] = $d->user_id;
               $item['creator_name'] = User::find($d->user_id)["username"];
               //echo $d->form_data;c
               $item['data'] = unserialize($d->form_data);
               $item['created_at'] = $d->created_at;
               $item['status'] = $d->status;
               $item['id'] = $d->id;
               if (array_key_exists('type',$item['data'])) {
                    $types = $item['data']['type'];
                    $formNameStr = "";
                    foreach ($types as $i => $tp) {
                        if ($tp == true) {
                           $formNameStr.= $i.", ";
                        }
                    }

                    $item['types'] = substr_replace($formNameStr,"","-2");
               }

               $forms[] = $item;
            }
            return Response::json(array("success" => "success", "formData" => $forms, "userStatus" => $userStatus));
        }
    }

    public function deleteForm()
    {
       $id = Request :: input("id");
       if ($id) {
           $data = FormData::find($id);
           $data->delete();
           return Response::json(array("success" => true, "message" => "item Deleted"));
       } else {
           return Response::json(array("success" => false, "message" => "Some Error!"));
       }
    }

    public function getScheduleGoods()
    {
      $schedule_goods = ScheduleGoods::all();
      $goodsList = array();
      foreach ($schedule_goods as $sg) {
        if ($sg->group != 'Misc') {
          $goodsList[] = array(
                               "group" => $sg->group,
                               "item"=> $sg->group." ".$sg->item,
                               "price" => $sg->rrp,
                               "category" => $sg->category,
                               "model" => $sg->model,
                               "cpiBw" => $sg->cpiBw,
                               "cpiColour" => $sg->cpiColour,
                               'features'=>$sg->features,
                               'descriptions'=>$sg->descriptions,
                               'hardware_solutions'=>$sg->hardware_solutions
                              );
        }
        else {
            $goodsList[] = array(
                               "group" => $sg->group,
                               "item"=> $sg->item,
                               "price" => $sg->rrp,
                               "category" => $sg->category,
                               "model" => $sg->model,
                               "cpiBw" => $sg->cpiBw,
                               "cpiColour" => $sg->cpiColour,
                               'features'=>$sg->features,
                               'descriptions'=>$sg->descriptions,
                               'hardware_solutions'=>$sg->hardware_solutions
                              );
        }
      }
       return Response::json(array("success" => true, "goods" => $goodsList));
    }
    public function showPaycorp()
    {
      $config = Config::get('app.paycorp');
      /*------------------------------------------------------------------------------
      STEP1: Build ClientConfig object
      ------------------------------------------------------------------------------*/
      $ClientConfig = new ClientConfig();
      $ClientConfig->setServiceEndpoint($config['endpoint']);
      $ClientConfig->setAuthToken($config['authToken']);
      $ClientConfig->setHmacSecret($config['hmac']);
      $ClientConfig->setValidateOnly(FALSE);
      /*------------------------------------------------------------------------------
      STEP2: Build Client object
      ------------------------------------------------------------------------------*/
      $Client = new GatewayClient($ClientConfig);
      /*------------------------------------------------------------------------------
      STEP3: Build PaymentInitRequest object
      ------------------------------------------------------------------------------*/
      $initRequest = new PaymentInitRequest();
      $initRequest->setClientId($config['clientID']);
      $initRequest->setTransactionType(TransactionType::$TOKEN);
      $initRequest->setClientRef("merchant_token_reference");
      $initRequest->setComment("merchant_additional_data");
      $initRequest->setTokenize(TRUE);
      //$initRequest->setExtraData(array("ADD-KEY-1" => "ADD-VALUE-1", "ADD-KEY-2" => "ADD-VALUE-2"));
      $initRequest->setCssLocation1('https://nexgenconfigapp.xyz/assets/css/form.css');
      // sets transaction-amounts details (all amounts are in cents)
      $transactionAmount = new TransactionAmount(0,"AUD");
      $transactionAmount->setTotalAmount(0);
      $transactionAmount->setServiceFeeAmount(0);
      $transactionAmount->setPaymentAmount(0);
      $transactionAmount->setCurrency("AUD");
      $initRequest->setTransactionAmount($transactionAmount);
      // sets redirect settings
      $redirect = new PaycorpRedirect($config['redirectUrl']);
      //$redirect->setReturnUrl();
      $redirect->setReturnMethod("POST");
      $initRequest->setRedirect($redirect);

      /*------------------------------------------------------------------------------
      STEP4: Process PaymentInitRequest object
      ------------------------------------------------------------------------------*/
      $initResponse = $Client->payment()->init($initRequest);

      $paycorpResponse = array(
                                'reqId' => $initResponse->getReqid(),
                                'expireAt' => $initResponse->getExpireAt(),
                                'paymentPageUrl' => $initResponse->getPaymentPageUrl()
                                );
      return Response::json(array("success" => true, "response" => $paycorpResponse));
    }

    public function getToken(){
      Blade::setEscapedContentTags('<$', '$>');
      Blade::setContentTags('<$$', '$$>');
      $reqid = Request :: input("reqid");
      $clientRef = Request :: input("clientRef");
      $config = Config::get('app.paycorp');
      /*------------------------------------------------------------------------------
      STEP1: Build ClientConfig object
      ------------------------------------------------------------------------------*/
      $ClientConfig = new ClientConfig();
      $ClientConfig->setServiceEndpoint($config['endpoint']);
      $ClientConfig->setAuthToken($config['authToken']);
      $ClientConfig->setHmacSecret($config['hmac']);
      $ClientConfig->setValidateOnly(FALSE);

      /* ------------------------------------------------------------------------------
       STEP2: Build PaycorpClient object
       ------------------------------------------------------------------------------ */
      $client = new GatewayClient($ClientConfig);

      /* ------------------------------------------------------------------------------
       STEP3: Build PaymentCompleteRequest object
       ------------------------------------------------------------------------------ */
      $completeRequest = new PaymentCompleteRequest();
      $completeRequest->setClientId($config['clientID']);
      $completeRequest->setReqid($reqid);
      /* ------------------------------------------------------------------------------
       STEP4: Process PaymentCompleteRequest object
       ------------------------------------------------------------------------------ */
      $completeResponse = $client->payment()->complete($completeRequest);
      $creditCard = $completeResponse->getCreditCard();
      $paycorpResponse = array(
                                'holderName' => $creditCard->getHolderName(),
                                'cardNumber' => $creditCard->getNumber(),
                                'expiry' => $creditCard->getExpiry(),
                                'type' => $creditCard->getType(),
                                'token' => $completeResponse->getToken()
                                );
      Session::put('paycorpResponse',$paycorpResponse);
      return View::make('payment_complete')->with('paycorpResponse',$paycorpResponse);
      //return Response::json(array("success" => true, "response" => $paycorpResponse));
    }

    public function getPaycorpToken(){
      $creditCard = Session::get('paycorpResponse');
      $paycorpResponse = array(
        'holderName' => $creditCard['holderName'],
        'cardNumber' => $creditCard['cardNumber'],
        'expiry' => $creditCard['expiry'],
        'type' => $creditCard['type'],
        'token' => $creditCard['token']
      );
      return Response::json(array("success" => true, "response" => $paycorpResponse));
    }
}
