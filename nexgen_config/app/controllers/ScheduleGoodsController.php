<?php

class ScheduleGoodsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$sched_goods = ScheduleGoods::all();
		return Response::json($sched_goods);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function store()
	{
		//
		$input = Input::all();

		try{

			$c = new ScheduleGoods();

			$c->group				= $input['group'];
			$c->item				= $input['item'];
			$c->rrp					= $input['rrp'];
			$c->model 				= $input['model'];
			$c->category			= $input['category'];
			$c->features 			= $input['features'];
			$c->descriptions 		= $input['descriptions'];
			$c->hardware_solutions	= $input['hardware_solutions'];



			if($c->save())
				return Response::json(array('success' => true,'id'=>$c->id ,"message"=>'Product Saved successfully!'));
			else
				throw new \Exception("Could not save the Product");
		} catch(\Exception $e){
			return Response::json(array('success' => false,'message'=>$e->getMessage()));
		}
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	public function show($id)
	{
		$scheduleGoods = ScheduleGoods::find($id);
		return Response::json($scheduleGoods);
	}
	
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$c =  ScheduleGoods::find($id);
		$input = Input::all();

		try{

			$c->group				= $input['group'];
			$c->item				= $input['item'];
			$c->rrp					= $input['rrp'];
			$c->model 				= $input['model'];
			$c->category			= $input['category'];
			$c->features 			= $input['features'];
			$c->descriptions 		= $input['descriptions'];
			$c->hardware_solutions	= $input['hardware_solutions'];
            
                      
                                
			if($c->save())
				return Response::json(array('success' => true,'id'=>$c->id));
			else
				throw new \Exception("Could not save the Product");

		} catch(\Exception $e){
			return Response::json(array('success' => false,'message'=>$e->getMessage()));
		}

	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
	if(ScheduleGoods::find($id)->delete())
		return Response::json(array('success' => true));
	else
	 	return Response::json(array('success' => false));
	}



}
