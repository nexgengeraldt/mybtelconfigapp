<?php 
class FormController extends \BaseController
{
    public function createPdf()
    {   
        
        set_time_limit(0);        
        $forms = Request :: input("htmlData"); 
        $userDetails = Request :: input("userDetails");
        $fromEmail = "noreply@businesstelecom.com.au";
        $fromName = "Business Telecom"; 
        $subject = 'Welcome to Business Telecom';
        $toEmail = $userDetails["email"];
        $toName = $userDetails["name"];
        
        $mailData =    [
                      'user' => $toName, 
                      'userEmail' => $toEmail,
                      'name' => $fromName, 
                      'email'=> $fromEmail
                ];
       
        $paths = [];
        foreach ($forms as $form) {
             $pdf = new mPDF('utf-8', 'A4');  
            $pdf->WriteHTML($form['html']);
             $data = array("pdf" => $pdf->Output("assets/files/".$form['fileName']));    
             $paths[] = "assets/files/".$form['fileName'];
        }  
        
        Mail::send('emails.form.forms', $mailData, function($message) use ($fromEmail, $fromName,
                    $subject, $paths)
        {
             $message->to('william@webmarketers.com.au', "William Bakhos")
                     ->to("naveen@webmarketers.com.au", "Naveen Jose")
                     ->cc("JamesHarb@nexgen.com.au", "James Harb");
            $message->from($fromEmail, $fromName);
            $message->subject($subject);
            foreach ($paths as $path) {
                $message->attach($path);
            }
        });
         return Response::json(array("success" => "success"));  
    }
    
    public function saveData()
    {
        $data = Request :: input("data");
        $user = Request :: input("user");
        $status = Request :: input("type");
        $formData = serialize($data);
        $editId = Request :: input("editId");
        if ($editId) {
            $fd = FormData::find($editId);
        } else {
            $fd = new FormData(); 
        }
        try {
            $fd->user_id = $user;
            $fd->form_data = $formData;
            $fd->status = $status;
            $fd->save();
            return Response::json(array("success" => "success", "editId"=> $fd->id));
        } catch (Exception $ex) {
            return Response ::json(array("success" => "false", "message" => $ex->getMessage()));
        }   
    }
    
    public function showForms()
    {
        $id = Request :: input("id");  
        $user_id = Request :: input("user");
        if ($id) {
           $data = FormData::find($id);
           $forms['data'] = unserialize($data['form_data']);
           $forms['id'] = $data['id'];
           return Response::json(array("success" => "success", "formData" => $forms));
        } else {  
            $userStatus = User::find($user_id)["type"];
            if ($userStatus == "admin") {
                $data = DB::table('form_data')->orderBy('created_at', "desc")
                                              ->where('user_id', $user_id)
                                              ->get(); 
            } else {
                $data = DB::table('form_data')->orderBy('created_at', "desc")
                                          ->get(); 
            }
            $forms = array();
            foreach ($data as $d) {     
               $item['creator_id'] = $d->user_id; 
               $item['creator_name'] = User::find($d->user_id)["username"];
               $item['data'] = unserialize($d->form_data);
               $item['created_at'] = $d->created_at;  
               $item['status'] = $d->status;
               $item['id'] = $d->id;
               if (array_key_exists('type',$item['data'])) {
                    $types = $item['data']['type'];
                    $formNameStr = "";                    
                    foreach ($types as $i => $tp) {
                        if ($tp == true) {               
                           $formNameStr.= $i.", ";
                        }
                    }
                 
                    $item['types'] = substr_replace($formNameStr,"","-2");
               }
               
               $forms[] = $item;
            }
            return Response::json(array("success" => "success", "formData" => $forms));
        }
    }
    
    public function deleteForm()
    {
       $id = Request :: input("id");       
       if ($id) {
           $data = FormData::find($id);
           $data->delete();
           return Response::json(array("success" => true, "message" => "item Deleted"));
       } else {
           return Response::json(array("success" => false, "message" => "Some Error!"));
       }
    }
}